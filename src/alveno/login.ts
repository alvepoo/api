import {Browser} from 'puppeteer';
import {LOGIN_URL, DASHBOARD_URL} from './core';


export declare interface LoginData
{
	auth: string,
	sid: string,
}


export async function login(browser: Browser, email: string, password: string): Promise<LoginData>
{
	const page = await browser.newPage();
	await page.goto(LOGIN_URL);

	const formEmail = await page.$('form#loginForm input[name="Login"]');
	const formPassword = await page.$('form#loginForm input[name="Password"]');
	const formSubmit = await page.$('form#loginForm button');

	await formEmail.type(email);
	await formPassword.type(password);
	await formSubmit.click();
	await page.waitForNavigation();

	if (page.url() !== DASHBOARD_URL) {
		const alert = await page.$('div.alert p');
		let alertText = 'AlvenoLoginError: ';

		if (alert) {
			alertText += (await (await alert.getProperty('textContent')).jsonValue()).toString().trim();
		} else {
			alertText += 'unknown error';
		}

		throw new Error(alertText);
	}

	const cookies = await page.cookies();
	const data: LoginData = {
		auth: null,
		sid: null,
	};

	cookies.map((cookie) => {
		if (cookie.name === 'ALVENOAUTH') {
			data.auth = cookie.value;
		} else if (cookie.name === 'ALVENOSID') {
			data.sid = cookie.value;
		}
	});

	return data;
}
