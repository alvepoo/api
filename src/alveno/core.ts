export const BASE_URL = 'https://klient.alveno.cz';
export const LOGIN_URL = `${BASE_URL}/Ucty/Ucty/Prihlaseni`;
export const DASHBOARD_URL = `${BASE_URL}/Dashboard/Dashboard/Dashboard`;
