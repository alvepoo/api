import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as logger from 'koa-logger';
import * as bodyParser from 'koa-bodyparser';
import * as puppeteer from 'puppeteer';

import * as alveno from './alveno';


(async () => {
	const browser = await puppeteer.launch();

	const app = new Koa;
	const router = new Router;

	router.get('/', (ctx) => {
		ctx.body = 'hello world';
	});

	router.post('/api/login', async (ctx) => {
		const data = ctx.request.body;

		if (typeof data.email !== 'string') {
			return ctx.throw(400, 'Missing or invalid email');
		}

		if (typeof data.password !== 'string') {
			return ctx.throw(400, 'Missing or invalid password');
		}

		try {
			ctx.body = {
				data: await alveno.login(browser, data.email, data.password),
			};
		} catch (e) {
			ctx.throw(403, {
				error: e.message,
			});
		}
	});

	app.use(logger());
	app.use(bodyParser());
	app.use(router.routes());
	app.use(router.allowedMethods());

	app.listen(8080);
})();
